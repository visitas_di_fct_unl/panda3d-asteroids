![FCTUNL](http://www.fct.unl.pt/sites/default/themes/fct_unl_pt_2015/images/logo.png)

# [Departamento de Informática](http://www.di.fct.unl.pt)

# Desenvolvimento de Jogos com Python - FCT Escolas 

Fernando Birra (fpb@fct.unl.pt)   e   João Costa Seco (joao.seco@fct.unl.pt)

![Python](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)

Nesta actividade convidamo-los a perceber a lógica de funcionamento
de um jogo de computador e aprender alguns conceitos básicos de programação. 
O que propomos é que siga um conjunto de passos simples, que primeiro explicam
os elementos base de um programa escrito na linguagem Python, e que depois levam
à construção de um pequeno jogo de computador, a partir de alguns componentes pré-preparados

# Programação 101

Vamos primeiro falar um pouco sobre programação. Será que é possível em tão pouco espaço e tempo?
Construir um programa é essencialmente especificar o comportamento de um computador, de forma a 
atingir os fins a que nos propomos. Para tal utiliza-se uma linguagem de programação, onde conseguimos 
exprimir esse comportamento utilizando conceitos como valores, variáveis, funções, objectos e classes. 
A linguagem que vamos utilizar chama-se Python, cujas ferramentas podem ser instaladas a partir do site 
[www.python.org](https://www.python.org). 

A maior parte dos programas e sistemas informáticos são construídos utilizando componentes pré-fabricados, 
e programando o novo comportamento utilizando esses componentes como blocos de construção. A combinação
desses blocos, e a construção de novos blocos é feita com novos programas, escritos numa linguagem
de programação. Para construir o nosso jogo hoje, vamos utilizar algumas desses componentes, um chamado 
"game engine", que nos vai facilitar muito a vida. Chama-se Panda3D e pode ser obtido a partir do site 
[www.panda3d.org](https://www.panda3d.org).

Vamos começar com o jogo Asteroids lançado pela Atari no início dos anos 80, e cujo aspecto ancestral é este:

![Game Image](https://bytebucket.org/costaseco/panda3d-asteroids/raw/6ce924eafe9143ab663b93e699d315417733caf9/panda3d%20Asteroids.png)

## Variáveis, Valores e operações

A linguagem python manipula valores que podem ser inteiros, reais, entre muitos outros. O jogo que vamos
desenvolver hoje é um jogo de naves, com algumas regras, física, aventura e diversão. A melhor maneira de 
conhecer os valores básicos de uma função é experimentar algumas expressões e valores.

Para experimentar algumas expressões é preciso executar um programa chamado "interpretador", ou seja 
neste caso, chama-se **python**.

Para começar, precisamos de saber onde é que a nossa nave está no jogo. Vamos definir duas variáveis, uma 
para a coordenada `x`, e outra para a coordenada `y` da nave.

~~~~
x = 10
y = 20
~~~~

Estes comandos da linguagem python garantem que o nome `x` fica agora associado ao valor `10`, e que o nome `y` 
fica associado ao valor `20`. Estes dois nomes podem representar a posição de uma nave em relação a um qualquer 
referencial. Certo?

Precisamos também ter uma representação para o valor da velocidade, e a direção de deslocação da nave. por exemplo,

~~~~
v = 5
dir = 30
~~~~

Para construirmos um jogo, precisamos que a nave se movimente de acordo com as regras da física. Ou seja, precisamos
de dar novos valores à posição da nave, passada uma unidade de tempo. Ou seja, podemos executar os seguintes comandos da linguagem

~~~~
from math import sin, cos, pi
DEG_TO_RAD = pi / 180
x = x + v*cos(dir*DEG_TO_RAD)
y = y + v*sin(dir*DEG_TO_RAD)
~~~~

Para sabermos qual o valor corrente de `x` e `y` podemos usar os comandos

~~~~
print(x)
print(y)
~~~~

Para actualizarmos a posição para mais uma unidade de tempo precisamos de repetir os comandos acima, certo? uma vez por cada unidade. 
Se variarmos o valor da variável `dir` teremos a nave a desenhar uma curva. Podemos dar um nome a esta actualização que 
queremos repetir, e mais ainda, fazê-la depender do intervalo de tempo que queremos considerar.

## Funções

Podemos definir o que se chama uma função

~~~~
def move(dt):
    global x
    global y
    x = x + v*cos(dir*DEG_TO_RAD)*dt
    y = y + v*sin(dir*DEG_TO_RAD)*dt
~~~~

o que nos permite movimentar a nave repetidamente, sem termos que escrever/repetir um comando complicado. Usamos

~~~~
move(10)
~~~~

para ver o efeito de mover a nave 10 unidades de tempo.

## Objectos 

Podemos juntar esta informação toda e construir o que se chama um objecto. Esta construção faz-se através da definição 
do que se chama uma classe, que consiste na especificação de um conjunto de objectos, todos com a mesma estrutura e com a mesma funcionalidade. A esses objetos chamamos de instâncias. Eis uma definição possível para a classe que representaria naves espaciais do nosso jogo:

~~~~
class Ship():
    def __init__(self,x,y,v,dir):
        self.x = x
        self.y = y
        self.v = v
        self.dir = dir
    
    def move(self, dt):
        self.x = self.x + self.v*cos(self.dir*DEG_TO_RAD)*dt
        self.y = self.y + self.v*sin(self.dir*DEG_TO_RAD)*dt
        
    def show(self):
        print(self.x)
        print(self.y)
        
    def turn(self,deg):
        self.dir = self.dir + deg
~~~~

Esta classe pode pois ser usada para criar objectos, tal como se vê na primeira linha do seguinte trecho do programa:

~~~~
ship = Ship(10,20,5,30)
ship.show()
ship.move(10)
ship.show()
ship.turn(90)
ship.move(10)
ship.show()
~~~~

As restantes linhas correspondem a operações que os objetos sabem efetuar. Cada operação é especificada por uma função definida anteriormente na classe Ship.

Classes e objectos podem ser usados para criar abstracções de problemas do mundo real (naves, asteroides, tiros, etc.).

# Jogos

Um Jogo de *Arcade* não é mais do que um cenário, neste caso a duas dimensões, onde vários objectos se movimentam segundo
algumas regras como aquelas que vimos acima, e onde o tempo é simulado, e daí os objectos se movimentarem "sozinhos". Pode-se 
encontrar na página [https://bitbucket.org/costaseco/panda3d-asteroids/src](https://bitbucket.org/costaseco/panda3d-asteroids/src) 
o jogo que vamos desenvolver aqui, e que podes descarregar, estudar, modificar, estender, etc. Durante a actividade ou em casa.

## Um Jogo de Asteroides

O Ficheiro que temos como primeiro passo é um dos exemplos da plataforma Panda3D que modificámos para ser mais simples de perceber 
e modificar. O ficheiro original tem o nome `0-main.py`, que podem descarregar e experimentar. Este código implementa a versão 
original do jogo e pode ser executado através do interpretador de `python` que já está instalado nos computadores. O código é algo
complicado de perceber na totalidade, mas está bem comentado e é bastante legível. Vamos focar em alguns aspectos mais centrais de 
forma a aprender os conceitos base. 

## Main Loop

Observando o ficheiro original podemos observar que existe uma classe chamada `AsteroidsDemo` que contém o código do Jogo (linha 179), 

~~~~
class AsteroidsDemo(ShowBase):
    def __init__(self):
        ShowBase.__init__(self)

    ...
~~~~

A classe `AsteroidsDemo` é aqui associada a uma outra classe (`ShowBase`) que faz parte do tal *game engine* Panda3D.

Continuando, o Jogo em si é iniciado pelas linhas no fim do ficheiro (linha 436). Parecido com o que vimos no início, certo?

~~~~
demo = AsteroidsDemo()
demo.run()
~~~~

Neste exemplo estamos a usar o Panda3D que implementa a parte essencial de controlo do Jogo, o chamado "Main Loop". Isto quer dizer 
que o jogo é feito pela repetição da execução da função que actualiza o estado do jogo, neste caso chama-se mesmo `gameLoop` (linha 316). 
É semelhante ao que fizemos anteriormente para a nossa nave (e com a função move).

## Actores

Para além de muitos detalhes, que estão mais ou menos explicados em comentários (linhas que começam por `#`), há um conjunto de classes 
que representam os vários Actores no nosso jogo (Ship, Asteroid, Bullet). 

~~~~
class Asteroid(MyActor):
    def __init__(self):
        MyActor.__init__(self, ASTEROID_FILENAME % (randint(1, 3)),
                                  scale=AST_INIT_SCALE)

class Bullet(MyActor):
    def __init__(self):
        MyActor.__init__(self, BULLET_FILENAME, scale=.2)
        

class Ship(MyActor):
    def __init__(self):
        MyActor.__init__(self,SHIP_FILENAME)
        
    ...
~~~~

Repare-se que a classe ship tem mais código, em particular a função `update` que vai ser usada para actualizar o estado da nave. 

Há muito mais para explicar e explorar, vamos passo a passo. E depois vamos enfrentar uns desafios.

# Desafios

Partindo deste jogo inicial temos uns desafios para vos colocar. Todos eles estão já resolvidos nos vários ficheiros que encontram 
junto desta página, mas não vale espreitar já já.

## Modificar o aspecto gráfico

O aspecto gráfico deste jogo é uma imitação de um jogo que existia no início dos anos 80. Já está um pouco desactualizado. O que nos
interessa aqui é que o aspecto dos actores é controlado através de imagens que são parte dos recursos de um jogo. Se modificarmos as 
linhas 49 a 52, para o seguinte código

~~~~
ASTEROID_FILENAME = "asteroid-%d.png"
BULLET_FILENAME = "bullet.png"
SHIP_FILENAME = "rocket-ship.png"
BACKGROUND_FILENAME = "sparkling-stars.jpg"
~~~~

estamos a utilizar umas imagens diferentes para construir o jogo. Que tal?

A nossa nave ficou um pouco deformada, não foi? Precisamos de ajustar a escala da figura. Na linha 130, no início da classe `Ship` 
vamos colocar os comandos 

~~~~
self.setSx(1)
self.setSz(2)
~~~~

que ficaria com o seguinte aspecto

~~~~
class Ship(MyActor):
    def __init__(self):
        MyActor.__init__(self,SHIP_FILENAME)
        self.setVelocity(LVector3.zero())
        self.setSx(1)
        self.setSz(2)

        self.keys = {"turnLeft": 0, 
                     "turnRight": 0,
                     "accel": 0, 
                     "fire": 0}
                     
    ...
~~~~

Agora sim, o jogo ficou com melhor aspecto.

(Se não conseguiste, a solução para este passo é o ficheiro `1-main.py`)

## Modificar o comportamento dos actores

O desafio seguinte, é modificar um pouco o comportamento de um dos actores, neste caso a nave. Queremos que a nave mostre quando está a acelerar. 
Por isso sempre que o jogador carrega na tecla *seta para cima* a imagem da nave deve modificar-se de forma a ter um jacto encarnado.

A primeira coisa é passarmos a conhecer mais do que uma imagem para a nave. Para isso precisamos de modificar a linha 51 para

~~~~
SHIP_FILENAME = "ship-no-thrust.png"
SHIP_FILENAME_THRUST = "ship-with-thrust.png"
~~~~

e a seguir à linha 137, na função __init__ da class Ship, os comandos

~~~~
self.tex_normal = loader.loadTexture("textures/" + SHIP_FILENAME)
self.tex_thrust = loader.loadTexture("textures/" + SHIP_FILENAME_THRUST)
~~~~

que quer dizer que passamos a conhecer duas "texturas" diferentes para a nave. Para as modificar, precisamos de uma função dentro da classe Ship (`thrust`), que pode ser colocada depois da função `update`

~~~~
    def thrust(self, yes):
        if yes:
            self.setTexture(self.tex_thrust,1)
        else:
            self.setTexture(self.tex_normal,1)
~~~~

E depois precisamos de usar esta função caso o jogador pressione a tecla de acelerar (seta para cima). Para isso colocamos na parte positiva do "if" o comando `self.thrust(True)` e na parte negativa (else), o comando `self.thrust(False)`

o aspecto final será:

~~~~
        if self.keys["accel"]:
            ...
            self.setVelocity(newVel)
            
            self.thrust(True)
        else:
            self.thrust(False)
~~~~

Agora sim, a nave acelera em grande estilo

(Se não conseguiste, a solução para este passo é o ficheiro `2-main.py`)

## Estender o Jogo com PowerUp

Nesta altura já percebemos como tudo isto funciona, o desafio agora é colocar algures no espaço um prémio, que faz com que a nave seja muito melhor e dispare 5 tiros simultaneamente em várias direções. Será que consegues? Provavelmente isto já é trabalho para um pouco mais de tempo. Estás à vontade para tentar e fazer as perguntas que quiseres.

(Se não conseguiste, a solução para este passo é o ficheiro `3-main.py`)


# Referências

[Documentação Panda 3D - Exemplo Asteroids](https://www.panda3d.org)