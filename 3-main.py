#!/usr/bin/env python

# Author: Shao Zhang, Phil Saltzman, and Greg Lindley
# Last Updated: 2015-03-13
#
# This tutorial demonstrates the use of tasks. A task is a function that
# gets called once every frame. They are good for things that need to be
# updated very often. In the case of asteroids, we use tasks to update
# the positions of all the objects, and to check if the bullets or the
# ship have hit the asteroids.
#
# Note: This definitely a complicated example. Tasks are the cores of
# most games so it seemed appropriate to show what a full game in Panda
# could look like.

# This example was adapted from the original example (authors above)
# Adaptation by: Fernando Birra and Joao Costa Seco
# Last update: 2016-02-25

from direct.showbase.ShowBase import ShowBase
from panda3d.core import TextNode, TransparencyAttrib, NodePath
from panda3d.core import LPoint3, LVector3
from direct.gui.OnscreenText import OnscreenText
from direct.task.Task import Task
from math import sin, cos, pi
from random import randint, choice, random
from direct.interval.MetaInterval import Sequence
from direct.interval.FunctionInterval import Wait, Func
import sys

# Constants that will control the behavior of the game. It is good to
# group constants like this so that they can be changed once without
# having to find everywhere they are used in code
SPRITE_POS = 55     # At default field of view and a depth of 55, the screen
# dimensions is 40x30 units
SCREEN_X = 20       # Screen goes from -20 to 20 on X
SCREEN_Y = 15       # Screen goes from -15 to 15 on Y
TURN_RATE = 360     # Degrees ship can turn in 1 second
ACCELERATION = 10   # Ship acceleration in units/sec/sec
MAX_VEL = 6         # Maximum ship velocity in units/sec
MAX_VEL_SQ = MAX_VEL ** 2  # Square of the ship velocity
DEG_TO_RAD = pi / 180  # translates degrees to radians for sin and cos
BULLET_LIFE = 2     # How long bullets stay on screen before removed
BULLET_REPEAT = .2  # How often bullets can be fired
BULLET_SPEED = 10   # Speed bullets move
AST_INIT_VEL = 1    # Velocity of the largest asteroids
AST_INIT_SCALE = 3  # Initial asteroid scale
AST_VEL_SCALE = 2.2  # How much asteroid speed multiplies when broken up
AST_SIZE_SCALE = .6  # How much asteroid scale changes when broken up
AST_MIN_SCALE = 1.1  # If and asteroid is smaller than this and is hit,
                     # it disapears instead of splitting up
POWERUP_INTERVAL = 30
POWERUP_EXPIRES = 10
SHIP_POWERUP = 15

ASTEROID_FILENAME = "asteroid-%d.png"
BULLET_FILENAME = "bullet.png"
SHIP_FILENAME = "ship-no-thrust.png"
SHIP_FILENAME_THRUST = "ship-with-thrust.png"
SHIP_FILENAME_UP = "ship-no-thrust-power.png"
SHIP_FILENAME_THRUST_UP = "ship-with-thrust-power.png"
BACKGROUND_FILENAME = "sparkling-stars.jpg"
POWERUP_FILENAME = "power-up.png"
    
# This class helps reduce the amount of code used by loading objects, since all of
# the objects are pretty much the same.    
class MyActor(NodePath):
    def __init__(self, tex=None, pos=LPoint3(0, 0), depth=SPRITE_POS, scale=1,
               transparency=True):
        # load the model
        obj = loader.loadModel("models/plane")
        # initialize the object from the loaded model
        NodePath.__init__(self, obj)
        self.reparentTo(camera)
        
        # Set the initial position and scale.
        self.setPos(pos.getX(), depth, pos.getY())
        self.setScale(scale)

        # This tells Panda not to worry about the order that things are drawn in
        # (ie. disable Z-testing).  This prevents an effect known as Z-fighting.
        self.setBin("unsorted", 0)
        self.setDepthTest(False)

        if transparency:
            # Enable transparency blending.
            self.setTransparency(TransparencyAttrib.MAlpha)

        if tex:
            # Load and set the requested texture.
            tex = loader.loadTexture("textures/" + tex)
            self.setTexture(tex, 1)

    def setVelocity(self, val):
        self.velocity = val

    def getVelocity(self):
        return self.velocity
        
    def setExpires(self, val):
        self.expires = val

    def getExpires(self):
        return self.expires
    
    def updatePos(self, dt):
        vel = self.getVelocity()
        newPos = self.getPos() + (vel * dt)

        # Check if the object is out of bounds. If so, wrap it
        radius = .5 * self.getScale().getX()
        if newPos.getX() - radius > SCREEN_X:
            newPos.setX(-SCREEN_X)
        elif newPos.getX() + radius < -SCREEN_X:
            newPos.setX(SCREEN_X)
        if newPos.getZ() - radius > SCREEN_Y:
            newPos.setZ(-SCREEN_Y)
        elif newPos.getZ() + radius < -SCREEN_Y:
            newPos.setZ(SCREEN_Y)

        self.setPos(newPos)
        
    def collidesWith(self, other):
        return (self.getPos() - other.getPos()).lengthSquared() < \
                    (((self.getScale().getX() + other.getScale().getX()) * .5) ** 2)


class Asteroid(MyActor):
    def __init__(self):
        MyActor.__init__(self, ASTEROID_FILENAME % (randint(1, 3)),
                                  scale=AST_INIT_SCALE)

class Bullet(MyActor):
    def __init__(self):
        MyActor.__init__(self, BULLET_FILENAME, scale=.2)
        

class Ship(MyActor):
    def __init__(self):
        MyActor.__init__(self,SHIP_FILENAME)
        self.setVelocity(LVector3.zero())
        self.setSx(1)
        self.setSz(2)
        self.poweredUp = False
        self.UPTime = 0

        self.keys = {"turnLeft": 0, 
                     "turnRight": 0,
                     "accel": 0, 
                     "fire": 0}
        
        self.tex_normal = loader.loadTexture("textures/" + SHIP_FILENAME)
        self.tex_thrust = loader.loadTexture("textures/" + SHIP_FILENAME_THRUST)
        self.tex_normalup = loader.loadTexture("textures/" + SHIP_FILENAME_UP)
        self.tex_thrustup = loader.loadTexture("textures/" + SHIP_FILENAME_THRUST_UP)

    def setKey(self, key, val):
        self.keys[key] = val

    def update(self, dt):
        heading = self.getR()  # Heading is the roll value for this model
        # Change heading if left or right is being pressed
        if self.keys["turnRight"]:
            heading += dt * TURN_RATE
            self.setR(heading % 360)
        elif self.keys["turnLeft"]:
            heading -= dt * TURN_RATE
            self.setR(heading % 360)

        # Thrust causes acceleration in the direction the ship is currently
        # facing
        if self.keys["accel"]:
            heading_rad = DEG_TO_RAD * heading
            # This builds a new velocity vector and adds it to the current one
            # relative to the camera, the screen in Panda is the XZ plane.
            # Therefore all of our Y values in our velocities are 0 to signify
            # no change in that direction.
            newVel = LVector3(sin(heading_rad), 0, cos(heading_rad)) * ACCELERATION * dt
            
            newVel += self.getVelocity()
            # Clamps the new velocity to the maximum speed. lengthSquared() is
            # used again since it is faster than length()
            if newVel.lengthSquared() > MAX_VEL_SQ:
                newVel.normalize()
                newVel *= MAX_VEL
            self.setVelocity(newVel)
            self.thrust(True)
        else:
            self.thrust(False)

        # Finally, update the position as with any other object
        self.updatePos(dt)
        
        
    def thrust(self, yes):
        if yes:
            if self.poweredUp:
                self.setTexture(self.tex_thrustup,1)
            else:
                self.setTexture(self.tex_thrust,1)
        else:
            if self.poweredUp:
                self.setTexture(self.tex_normalup,1)
            else:
                self.setTexture(self.tex_normal,1)
                
            
    def powerUp(self, UPTime):
        self.poweredUp = True
        self.UPTime = UPTime

    def getUPTime(self):
        return self.UPTime
        
    def powerDown(self):
        self.poweredUp = False
        
    def isPoweredUp(self):
        return self.poweredUp
        
class PowerUp(MyActor):
    def __init__(self):
        MyActor.__init__(self, POWERUP_FILENAME)
        self.setX(choice(tuple(range(-SCREEN_X, -5)) + tuple(range(5, SCREEN_X))))
        self.setZ(choice(tuple(range(-SCREEN_Y, -5)) + tuple(range(5, SCREEN_Y))))
        self.setSx(2)
        self.setSz(2)
        

# Macro-like function used to reduce the amount to code needed to create the
# on screen instructions
def genLabelText(text, i):
    return OnscreenText(text=text, parent=base.a2dTopLeft, pos=(0.07, -.06 * i - 0.1),
                        fg=(1, 1, 1, 1), align=TextNode.ALeft, shadow=(0, 0, 0, 0.5), scale=.05)
    
class AsteroidsDemo(ShowBase):
    def __init__(self):
        # Initialize the ShowBase class from which we inherit, which will
        # create a window and set up everything we need for rendering into it.
        ShowBase.__init__(self)

        # This code puts the standard title and instruction text on screen
        self.title = OnscreenText(text="Panda3D: Tutorial - Tasks",
                                  parent=base.a2dBottomRight, scale=.07,
                                  align=TextNode.ARight, pos=(-0.1, 0.1),
                                  fg=(1, 1, 1, 1), shadow=(0, 0, 0, 0.5))
        self.escapeText = genLabelText("ESC: Quit", 0)
        self.leftkeyText = genLabelText("[Left Arrow]: Turn Left (CCW)", 1)
        self.rightkeyText = genLabelText("[Right Arrow]: Turn Right (CW)", 2)
        self.upkeyText = genLabelText("[Up Arrow]: Accelerate", 3)
        self.spacekeyText = genLabelText("[Space Bar]: Fire", 4)

        # Disable default mouse-based camera control.  This is a method on the
        # ShowBase class from which we inherit.
        self.disableMouse()

        # Load the background starfield.
        self.setBackgroundColor((0, 0, 0, 1))
        bg = MyActor(BACKGROUND_FILENAME, scale=146, depth=200,
                             transparency=False)

        # Load the ship
        self.ship = Ship()

        # A dictionary of what keys are currently being pressed
        # The key events update this list, and our task will query it as input
        self.keys = {"turnLeft": 0, "turnRight": 0,
                     "accel": 0, "fire": 0}

        self.accept("escape", sys.exit)  # Escape quits
        # Other keys events set the appropriate value in our key dictionary
        self.accept("arrow_left",     self.setKey, ["turnLeft", 1])
        self.accept("arrow_left-up",  self.setKey, ["turnLeft", 0])
        self.accept("arrow_right",    self.setKey, ["turnRight", 1])
        self.accept("arrow_right-up", self.setKey, ["turnRight", 0])
        self.accept("arrow_up",       self.setKey, ["accel", 1])
        self.accept("arrow_up-up",    self.setKey, ["accel", 0])
        self.accept("space",          self.setKey, ["fire", 1])

        # Now we create the task. taskMgr is the task manager that actually
        # calls the function each frame. The add method creates a new task.
        # The first argument is the function to be called, and the second
        # argument is the name for the task.  It returns a task object which
        # is passed to the function each frame.
        self.gameTask = taskMgr.add(self.gameLoop, "gameLoop")

        # Stores the time at which the next bullet may be fired.
        self.nextBullet = 0.0

        # This list will stored fired bullets.
        self.bullets = []

        # Complete initialization by spawning the asteroids.
        self.spawnAsteroids()
        self.activePowerUp = None
        self.nextPowerUp = 0

        
    def asteroidHit(self, index):
        # If the asteroid is small it is simply removed
        if self.asteroids[index].getScale().getX() <= AST_MIN_SCALE:
            # Hide the asteroid
            self.asteroids[index].removeNode()
            # Remove the asteroid from the list of asteroids.
            del self.asteroids[index]
        else:
            # If it is big enough, divide it up into little asteroids.
            # First we update the current asteroid.
            asteroid = self.asteroids[index]
            newScale = asteroid.getScale().getX() * AST_SIZE_SCALE

            # The new direction is chosen as perpendicular to the old direction
            # This is determined using the cross product, which returns a
            # vector perpendicular to the two input vectors.  By crossing
            # velocity with a vector that goes into the screen, we get a vector
            # that is orthagonal to the original velocity in the screen plane.
            vel = asteroid.getVelocity()
            speed = vel.length() * AST_VEL_SCALE
            vel.normalize()
            vel = LVector3(0, 1, 0).cross(vel)
            vel *= speed

            # Now we create a new asteroid identical to the current one
            newAst = MyActor(scale=newScale)
            newAst.setVelocity(vel)
            newAst.setPos(asteroid.getPos())
            newAst.setTexture(asteroid.getTexture(), 1)
            self.asteroids.append(newAst)      

            # Now we create a new asteroid identical to the current one
            newAst = MyActor(scale=newScale)
            newAst.setVelocity(vel * -1)
            newAst.setPos(asteroid.getPos())
            newAst.setTexture(asteroid.getTexture(), 1)
            self.asteroids.append(newAst)      

            # Hide the asteroid
            self.asteroids[index].removeNode()
            # Remove the asteroid from the list of asteroids.
            del self.asteroids[index]
        
        
    def spawnAsteroids(self):
        # Control variable for if the ship is alive
        self.alive = True
        self.asteroids = []  # List that will contain our asteroids

        for i in range(10):
            # This loads an asteroid.
            asteroid = Asteroid()
            self.asteroids.append(asteroid)

            # This is kind of a hack, but it keeps the asteroids from spawning
            # near the player.  It creates the list (-20, -19 ... -5, 5, 6, 7,
            # ... 20) and chooses a value from it. Since the player starts at 0
            # and this list doesn't contain anything from -4 to 4, it won't be
            # close to the player.
            asteroid.setX(choice(tuple(range(-SCREEN_X, -5)) + tuple(range(5, SCREEN_X))))
            # Same thing for Y
            asteroid.setZ(choice(tuple(range(-SCREEN_Y, -5)) + tuple(range(5, SCREEN_Y))))

            # Heading is a random angle in radians
            heading = random() * 2 * pi

            # Converts the heading to a vector and multiplies it by speed to
            # get a velocity vector
            v = LVector3(sin(heading), 0, cos(heading)) * AST_INIT_VEL
            self.asteroids[i].setVelocity(v)  
            
            
    def setKey(self, key, val):
        self.keys[key] = val
        self.ship.setKey(key, val)
        
        
    def gameLoop(self, task):
        # Get the time elapsed since the next frame.  We need this for our
        # distance and velocity calculations.
        dt = globalClock.getDt()

        # If the ship is not alive, do nothing.  Tasks return Task.cont to
        # signify that the task should continue running. If Task.done were
        # returned instead, the task would be removed and would no longer be
        # called every frame.
        if not self.alive:
            return Task.cont

        # update ship position
        self.ship.update(dt)

        # check to see if the ship can fire
        if self.keys["fire"] and task.time > self.nextBullet:
            self.fire(task.time)  # If so, call the fire function
            # And disable firing for a bit
            self.nextBullet = task.time + BULLET_REPEAT
        # Remove the fire flag until the next spacebar press
        self.keys["fire"] = 0

        # update asteroids
        for obj in self.asteroids:
            obj.updatePos(dt)

        # update bullets
        newBulletArray = []
        for obj in self.bullets:
            obj.updatePos(dt)  # Update the bullet
            # Bullets have an experation time (see definition of fire)
            # If a bullet has not expired, add it to the new bullet list so
            # that it will continue to exist.
            if obj.getExpires() > task.time:
                newBulletArray.append(obj)
            else:
                obj.removeNode()  # Otherwise, remove it from the scene.
        # Set the bullet array to be the newly updated array
        self.bullets = newBulletArray

        # Check bullet collision with asteroids
        # In short, it checks every bullet against every asteroid. This is
        # quite slow.  A big optimization would be to sort the objects left to
        # right and check only if they overlap.  Framerate can go way down if
        # there are many bullets on screen, but for the most part it's okay.
        for bullet in self.bullets:
            # This range statement makes it step though the asteroid list
            # backwards.  This is because if an asteroid is removed, the
            # elements after it will change position in the list.  If you go
            # backwards, the length stays constant.
            for i in range(len(self.asteroids) - 1, -1, -1):
                asteroid = self.asteroids[i]
                # Panda's collision detection is more complicated than we need
                # here.  This is the basic sphere collision check. If the
                # distance between the object centers is less than sum of the
                # radii of the two objects, then we have a collision. We use
                # lengthSquared() since it is faster than length().
                if (bullet.collidesWith(asteroid)):
                    # Schedule the bullet for removal
                    bullet.setExpires(0)
                    self.asteroidHit(i)      # Handle the hit

        # Now we do the same collision pass for the ship
        for ast in self.asteroids:
            # Same sphere collision check for the ship vs. the asteroid
            if (self.ship.collidesWith(ast)):
                # If there is a hit, clear the screen and schedule a restart
                self.alive = False         # Ship is no longer alive
                # Remove every object in asteroids and bullets from the scene
                for i in self.asteroids + self.bullets:
                    i.removeNode()
                self.bullets = []          # Clear the bullet list
                self.ship.hide()           # Hide the ship
                # Reset the velocity
                self.ship.setVelocity(LVector3(0, 0, 0))
                Sequence(Wait(2),          # Wait 2 seconds
                         Func(self.ship.setR, 0),  # Reset heading
                         Func(self.ship.setX, 0),  # Reset position X
                         # Reset position Y (Z for Panda)
                         Func(self.ship.setZ, 0),
                         Func(self.ship.show),     # Show the ship
                         Func(self.spawnAsteroids)).start()  # Remake asteroids
                return Task.cont

        # If the player has successfully destroyed all asteroids, respawn them
        if len(self.asteroids) == 0:
            self.spawnAsteroids()

        if self.activePowerUp is not None:
            if self.activePowerUp.getExpires() < task.time:
                self.activePowerUp.removeNode()
                self.activePowerUp = None
                
        if (self.activePowerUp is None) and (self.nextPowerUp < task.time):
            self.activePowerUp = PowerUp()
            self.activePowerUp.setExpires(task.time + POWERUP_EXPIRES)
            self.nextPowerUp = task.time + POWERUP_INTERVAL
            
        if (self.activePowerUp is not None) and self.ship.collidesWith(self.activePowerUp):
            self.activePowerUp.removeNode()
            self.activePowerUp = None
            self.ship.powerUp(task.time+SHIP_POWERUP)
            
        if self.ship.getUPTime() < task.time:
            self.ship.powerDown()
        
        return Task.cont    # Since every return is Task.cont, the task will
        # continue indefinitely 
    
    
    def fire(self, time):
        self.fireTo(self.ship.getR(),time)
        if self.ship.isPoweredUp(): 
            self.fireTo(self.ship.getR()+30,time)
            self.fireTo(self.ship.getR()-30,time)
            self.fireTo(self.ship.getR()+60,time)
            self.fireTo(self.ship.getR()-60,time)

        
    def fireTo(self,direction,time):
        direction = DEG_TO_RAD * direction
        pos = self.ship.getPos()
        #bullet = MyActor("bullet.png", scale=.2)  # Create the object
        bullet = Bullet()
        bullet.setPos(pos)
        # Velocity is in relation to the ship
        vel = (self.ship.getVelocity() +
               (LVector3(sin(direction), 0, cos(direction)) *
                BULLET_SPEED))
        bullet.setVelocity(vel)
        # Set the bullet expiration time to be a certain amount past the
        # current time
        bullet.setExpires(time + BULLET_LIFE)

        # Finally, add the new bullet to the list
        self.bullets.append(bullet) 
    
        
    
    
    

# We now have everything we need. Make an instance of the class and start
# 3D rendering


demo = AsteroidsDemo()
demo.run()
